#!/bin/bash
source ./config.sh
RUST_LOG=INFO cargo run relayer \
--cosmos-grpc="$cosmos_grpc" \
--ethereum-key="$eth_key" \
# --address-prefix="$prefix" \
--ethereum-rpc="$infura_rpc" \
--gravity-contract-address="$contract_address"
