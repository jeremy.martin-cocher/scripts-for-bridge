# Cosmos <> Ethereum bridge: JEMC's PoC

This repo contains the scripts that you have to run alongside the Cosmos blockchain's daemon in order to use the bridge.

There are 2 scripts: 

- The **Orchestrator**: monitors Ethereum bridge Smart Contract & sign tx to Ethereum with eth keys

- The **Relayer**: relay tx batch from Cosmos to Ethereum




## Step 1: Private parameters

First, create a private.sh file at the root of the folder and add your Ethreum private key and your Ethereum rpc node (can be infura).

it should look like this:

```
infura_rpc="{your_eth_rpc}"
eth_key="{your_eth_private_key}"
```


## Step 3: build the binaries

Then you can build the binaries with the command:

```
cargo build
```

## Step 4: gbt parameters

Gbt is used to associate an Ethereum private key to a Cosmos validator.

Do the command `gbt init`

And then set up your parameters with the gbt command:

```
gbt -a cosmos keys register-orchestrator-address \
--validator-phrase="{cosmos_mnemonic_of_the_validator}" \
--cosmos-phrase="{cosmos_mnemonic_of_the_orchestrator}" \
--ethereum-key="{ethereum_private_key}" \
--fees=125000token
```


## Step 5: launch the scritps

Now you are ready to start the scripts with

```
bash start-orchestrator.sh
```
and
```
bash start-relayer.sh
```
